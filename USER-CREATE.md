**USER CREATE API**
----

* **URL**

  /api/v1/user/add

* **Method:**
  
  `POST`
  
*  **URL Params**
No URL Params

*  **HEADER**
API-TOKEN:5CD4ED173E1C95FE763B753A297D5

* **Data Params**

| NAME | DESCRIPTION |
| ------ | ------ |
| avatar | avatar name|
| name   | user name|


 avatar=/avatar.png&name=prueba

* **Success Response:**
 
  * **Code:** 201 Created<br />
    **Content:** 
 ```json
{
	"error": false,

	"item": {

		"_id": "5e013ae59cb84346381c354d",

		"avatar": "http://www.clarin.com",

		"name": "prueba",

		"createdAt": "2019-12-23T22:08:37.801Z",

		"updatedAt": "2019-12-23T22:08:37.801Z",

		"__v": 0

	}

}
```
* **Error Response:**

 This will happen when the API-TOKEN header is missing or with an invalid value

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `401 Authorization Required`

  OR

This will happen when the **AVATAR** or **NAME** params are missing

  * **Code:** 500 Internal Server Error <br />
    **Content:** 
  ```json
   {

	"error": true,

	"statusCode": 500,

	"message": "Not able to create item",

	"errors": {

		"name": {

		"message": "Path `name` is required.",

		"name": "ValidatorError",

		"properties": {

			"message": "Path `name` is required.",

			"type": "required",

			"path": "name"

		},

		"kind": "required",

		"path": "name"

		}

	}

}
```
* **Sample Call:**

POST /api/v1/user/add HTTP/1.1
Host: localhost:3000
API-TOKEN: 5CD4ED173E1C95FE763B753A297D5
Content-Type: application/x-www-form-urlencoded
User-Agent: PostmanRuntime/7.20.1
Accept: */*
Cache-Control: no-cache
Postman-Token: b3953941-9734-4d19-b738-24adab859771,594d3846-f719-4e22-a40b-5c52ea0e686d
Host: localhost:3000
Accept-Encoding: gzip, deflate
Content-Length: 34
Connection: keep-alive
cache-control: no-cache

avatar=http%3A%2F%2Fwww.clarin.com

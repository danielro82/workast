FROM node:slim
RUN mkdir -p /home/node/workast
WORKDIR /home/node/workast
COPY package.json /home/node/workast
RUN npm install
RUN npm install pm2 -g
COPY . /home/node/workast
#CMD [ "node", "index.js" ]
#CMD ["npm", "start"]
CMD ["pm2-runtime", "start", "ecosystem.config.js"]
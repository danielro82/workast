
**GET ARTICLE  by TAGS API**

* **URL**

  /api/v1/article/get/?tags=

* **Method:**
  
  `GET`
  
*  **URL Params**
**TAGS** is a string separated by comma where each element between the comas is a tag 
**Example** "Soccer, News", Soccer is a Tag and News is another one.

*  **HEADER**
API-TOKEN:5CD4ED173E1C95FE763B753A297D5

* **Data Params**
No body params

* **Success Response:**
 
  * **Code:** 200 OK<br />
    **Content:** 
 ```json
{

	"error": false,

	"statusCode": 200,

	"data": [

	{

	"tags": [

		"2134",

		"123",

		"432423"

		],

		"_id": "5e00032aacae6e6b061cf3a1",

		"text": "es una prueba",

		"title": "prueba",

		"userId": "5dfebb906efbc825bd2c4f12",

		"createdAt": "2019-12-22T23:58:34.160Z",

		"updatedAt": "2019-12-22T23:58:34.160Z",

		"__v": 0

		},

		{

		"tags": [

			"2134",

			"123",

			"432423"

		],

		"_id": "5e000331acae6e6b061cf3a2",

		"text": "es una prueba",

		"title": "prueba",

		"userId": "5dfebb906efbc825bd2c4f12",

		"createdAt": "2019-12-22T23:58:41.281Z",

		"updatedAt": "2019-12-22T23:58:41.281Z",

		"__v": 0

		},

		{

		"tags": [

			"2134",

			"123",

			"432423"

		],

		"_id": "5e01248a585e5823c7d18512",

		"text": "es una prueba",

		"title": "prueba",

		"userId": "5dfebb906efbc825bd2c4f12",

		"createdAt": "2019-12-23T20:33:14.147Z",

		"updatedAt": "2019-12-23T20:33:14.147Z",

		"__v": 0

		},

		{

		"tags": [

			"2134",

			"123",

			"432423"

		],

		"_id": "5e01419f9cb84346381c3550",

		"text": "es una prueba",

		"title": "prueba",

		"userId": "5dfebb906efbc825bd2c4f12",

		"createdAt": "2019-12-23T22:37:19.404Z",

		"updatedAt": "2019-12-23T22:37:19.404Z",

		"__v": 0

		},

		{

		"tags": [
		
			"2134",

			"123",

			"432423"

		],

		"_id": "5e0141eb9cb84346381c3551",

		"text": "es una prueba",

		"title": "prueba",

		"userId": "5dfebb906efbc825bd2c4f12",

		"createdAt": "2019-12-23T22:38:35.974Z",

		"updatedAt": "2019-12-23T22:38:35.974Z",

		"__v": 0

		}

	],

"total": 5

}
```
* **Error Response:**

 This will happen when the API-TOKEN header is missing or with an invalid value

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `401 Authorization Required`

  OR
This will happen when the tags parameter is missed
 **Code:** 400 Bad Request <br />
    **Content:** 
```json
{

	"error": true,

	"statusCode": 500,

	"errors": [

		"Param tags is mandatory"

	]

}
```
This will happen when the **Article ID** is wrong
  * **Code:** 500 Internal Server Error <br />
    **Content:** 
```json
{

"error": {

	"stringValue": "\"5e01441e9cb84346381c355\"",

	"kind": "ObjectId",

	"value": "5e01441e9cb84346381c355",

	"path": "_id",

	"reason": {},

	"message": "Cast to ObjectId failed for value \"5e01441e9cb84346381c355\" at path \"_id\" for model \"articles\"",

	"name": "CastError"

	},

"statusCode": 500

}
```

* **Sample Call:**

GET /api/v1/article/get?tags=312312312 HTTP/1.1
Host: localhost:3000
API-TOKEN: 5CD4ED173E1C95FE763B753A297D5
User-Agent: PostmanRuntime/7.20.1
Accept: */*
Cache-Control: no-cache
Postman-Token: 10282165-edba-416e-afc4-3ffc284438d2,fcc069c8-e66d-4bf1-be0b-f2fbeb591a28
Host: localhost:3000
Accept-Encoding: gzip, deflate
Connection: keep-alive
cache-control: no-cache


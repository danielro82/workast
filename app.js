const express = require('express');
const pino = require('pino')();
const logger = require('express-pino-logger')({
  instance: pino,
});

const indexRouter = require('./routes/index');

const app = express();

/*
* AUTHENTICATION with authorization header
* default token 5CD4ED173E1C95FE763B753A297D5
*/
app.use(async (req, res) => {
  try {
    const apiToken = process.env.apiToken || '5CD4ED173E1C95FE763B753A297D5';
    const token = req.headers['api-token'];
    if (apiToken !== token) {
      res.status(401).send('401 Authorization Required');
    }
    return req.next();
  } catch (e) {
    res.status(500).send('500 Internal Server');
  }
});

app.use(logger);
app.use(express.json());
app.use(express.urlencoded({extended: false}));

const dev = process.env.NODE_ENV !== 'production';

if (!dev) app.set('trust proxy', 1);

app.use('/', indexRouter);

/*
*catch 404 and forward to error handler
*/
app.use((req, res, next) => {
  res.status(404).send('404 Not Found');
});

module.exports = app;

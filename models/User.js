// src/models/Post.js
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

/**
* User model _id, name string, avatar string
*/
class User {
  /**
  * Use singleton pattern
  */
  constructor() {
    if (!!User.instance) {
        return User.instance;
    }

    this.initSchema();
    User.instance = this;

    return this;
  }
  /**
   * Initialize the Mongoose Schema
   */
  initSchema() {
    const schema = new mongoose.Schema({
      name: {
        type: String,
        required: true,
      },
      avatar: {
        type: String,
        required: true,
      },
    }, {timestamps: true});
    schema.plugin(uniqueValidator);
    mongoose.model('users', schema);
  }
  /**
   * Get the users model
   * @return {model}
   */
  getModel() {
    return mongoose.model('users');
  }
}

module.exports = User;

// src/models/Post.js
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

/**
* User model _id, name string, avatar string
*/
class Article {
  /**
  * Use singleton pattern
  */
  constructor() {
    if (!!Article.instance) {
        return Article.instance;
    }

    this.initSchema();
    Article.instance = this;

    return this;
  }
  /**
   * Initialize the Mongoose Schema
   */
  initSchema() {
    const schema = new mongoose.Schema({
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
      },
      title: {
        type: String,
        required: true,
      },
      text: {
        type: String,
        required: true,
      },
      tags: {
        type: [{
          type: String,
        }],
        required: true,
      },
    }, {timestamps: true});
    schema.pre('save', function(next) {
      const article = this;
      article.tags = this.tags[0].split(',').map((el) => el.trim());
      next();
    });
    schema.plugin(uniqueValidator);
    mongoose.model('articles', schema);
  }
  /**
   * Get the users model
   * @return {model}
   */
  getModel() {
    return mongoose.model('articles');
  }
}

module.exports = Article;

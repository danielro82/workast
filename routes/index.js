const express = require('express');
const router = new express.Router();
require('express-group-routes');

let Article = require('./../models/Article');
Article = new Article();
const ArticleService = require('./../services/ArticleService');
const articleService = new ArticleService(
    Article.getModel(),
);
let User = require('./../models/User');
User = new User();
const UserService = require('./../services/UserService');
const userService = new UserService(
    User.getModel(),
);


const UsersController =
require('../controllers/usersController');
const usersController = new UsersController(userService);

const ArticlesController =
require('../controllers/articlesController');
const articlesController = new ArticlesController(articleService);
const Mongoose = require('../libraries/database');
const apicache = require('apicache');
const cache = apicache.middleware;

new Mongoose();
/*
* Routes, API version 1
*/
router.group('/api/v1', (router) => {
  router.post('/user/add', usersController.insert);
  router.post('/article/add', articlesController.insert);
  router.put('/article/edit/:id', articlesController.update);
  router.delete('/article/delete/:id', articlesController.delete);
  router.get('/article/get', cache('5 minutes'), articlesController.getAll);
});

module.exports = router;

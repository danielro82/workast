const Controller = require('./Controller');
/**
* Articles Controller
*/
class ArticlesController extends Controller {
  /**
   *
   * @param {*} service
   */
  constructor(service) {
    super(service);
  }
}

module.exports = ArticlesController;

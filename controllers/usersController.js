const Controller = require('./Controller');
/**
* Users Controller
*/
class UsersController extends Controller {
  /**
   *
   * @param {*} service
   */
  constructor(service) {
    super(service);
  }
}

module.exports = UsersController;


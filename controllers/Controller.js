/**
* Generic Controller
*/
class Controller {
  /**
   *
   * @param {*} service
   */
  constructor(service) {
    this.service = service;
    this.getAll = this.getAll.bind(this);
    this.insert = this.insert.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }
  /**
   * Get all mongo Documents
   * @param {*} req
   * @param {*} res
   */
  async getAll(req, res) {
    return res.status(200).send(await this.service.getAll(req.query));
  }

  /**
   * Create a mongo document
   * @param {*} req
   * @param {*} res
   */
  async insert(req, res) {
    const response = await this.service.insert(req.body);
    if (response.error) return res.status(response.statusCode).send(response);
    return res.status(201).send(response);
  }

  /**
   * Update a mongo Document
   * @param {*} req
   * @param {*} res
   */
  async update(req, res) {
    const {id} = req.params;

    const response = await this.service.update(id, req.body);

    return res.status(response.statusCode).send(response);
  }

  /**
   * Delete a mongo Document
   * @param {*} req
   * @param {*} res
   */
  async delete(req, res) {
    const {id} = req.params;

    const response = await this.service.delete(id);

    return res.status(response.statusCode).send(response);
  }
}

module.exports = Controller;


**ARTICLE EDIT API**

* **URL**

  /api/v1/article/edit/:id

* **Method:**
  
  `PUT`
  
*  **URL Params**
**ID** is the **Article ID**
**Example** 5dff8bd8c757a6620d117622

*  **HEADER**
API-TOKEN:5CD4ED173E1C95FE763B753A297D5

* **Data Params**

| NAME | DESCRIPTION |
| ------ | ------ |
| title | article title|
| text   | article body|
| userId   | User id (from Collection)|
| tags   | array of tags|

text=es+una+prueba&title=prueba&userId=5dfebb906efbc825bd2c4f12&tags=2134%2C+123%2C432423

* **Success Response:**
 
  * **Code:** 202 Accepted<br />
    **Content:** 
 ```json
{

"error": false,

"statusCode": 202,

"item": {

	"tags": [

	"2134",

	"123",

	"432423"

	],

	"_id": "5e01441e9cb84346381c3555",

	"text": "Modificado",

	"title": "prueba",

	"userId": "5dfebb906efbc825bd2c4f12",

	"createdAt": "2019-12-23T22:47:58.755Z",

	"updatedAt": "2019-12-23T22:48:07.882Z",

	"__v": 0

	}
}
```
* **Error Response:**

 This will happen when the API-TOKEN header is missing or with an invalid value

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `401 Authorization Required`

  OR

This will happen when the userId is not found
  * **Code:** 500 Internal Server Error <br />
    **Content:** 
```json
{

	"error": true,

	"statusCode": 500,

	"message": "Not able to create item",

	"errors": {

		"userId": {

			"stringValue": "\"5dfebb906efbc825bd2c4f1\"",

			"kind": "ObjectID",

			"value": "5dfebb906efbc825bd2c4f1",

			"path": "userId",

			"reason": {

				"stringValue": "\"5dfebb906efbc825bd2c4f1\"",

				"kind": "ObjectId",

				"value": "5dfebb906efbc825bd2c4f1",

				"path": "userId",

				"reason": {},

				"message": "Cast to ObjectId failed for value \"5dfebb906efbc825bd2c4f1\" at path \"userId\"",

				"name": "CastError"

				},

			"message": "Cast to ObjectID failed for value \"5dfebb906efbc825bd2c4f1\" at path \"userId\"",

			"name": "CastError"

		}

	}

}
```
OR

This will happen when the **Article ID** is wrong
  * **Code:** 500 Internal Server Error <br />
    **Content:** 
```json
{

"error": {

	"stringValue": "\"5e01441e9cb84346381c355\"",

	"kind": "ObjectId",

	"value": "5e01441e9cb84346381c355",

	"path": "_id",

	"reason": {},

	"message": "Cast to ObjectId failed for value \"5e01441e9cb84346381c355\" at path \"_id\" for model \"articles\"",

	"name": "CastError"

	},

"statusCode": 500

}

* **Sample Call:**


POST /api/v1/article/add HTTP/1.1
Host: localhost:3000
API-TOKEN: 5CD4ED173E1C95FE763B753A297D5
Content-Type: application/x-www-form-urlencoded
User-Agent: PostmanRuntime/7.20.1
Accept: */*
Cache-Control: no-cache
Postman-Token: ecc40560-4236-4769-9267-e012d5165c6a,9b532d28-0eec-46ba-a1bd-7383af95cfaa
Host: localhost:3000
Accept-Encoding: gzip, deflate
Content-Length: 72
Connection: keep-alive
cache-control: no-cache

title=prueba&userId=5dfebb906efbc825bd2c4f12&tags=2134%2C+123%2C432423
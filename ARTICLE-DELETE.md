
**ARTICLE DELETE API**

* **URL**

  /api/v1/article/delete/:id

* **Method:**
  
  `DELETE`
  
*  **URL Params**
**ID** is the **Article ID**
**Example** 5dff8bd8c757a6620d117622

*  **HEADER**
API-TOKEN:5CD4ED173E1C95FE763B753A297D5

* **Data Params**
No body params

* **Success Response:**
 
  * **Code:** 202 Accepted<br />
    **Content:** 
 ```json
{

"error": false,

"deleted": true,

"statusCode": 202,

"item": {

	"tags": [

		"2134",

		"123",

		"432423"

	],

	"_id": "5e01441e9cb84346381c3555",

	"text": "Modificado",

	"title": "prueba",

	"userId": "5dfebb906efbc825bd2c4f12",

	"createdAt": "2019-12-23T22:47:58.755Z",

	"updatedAt": "2019-12-23T22:48:07.882Z",

	"__v": 0

	}

}
```
* **Error Response:**

 This will happen when the API-TOKEN header is missing or with an invalid value

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `401 Authorization Required`

  OR

This will happen when the **Article ID** is wrong
  * **Code:** 500 Internal Server Error <br />
    **Content:** 
```json
{

"error": {

	"stringValue": "\"5e01441e9cb84346381c355\"",

	"kind": "ObjectId",

	"value": "5e01441e9cb84346381c355",

	"path": "_id",

	"reason": {},

	"message": "Cast to ObjectId failed for value \"5e01441e9cb84346381c355\" at path \"_id\" for model \"articles\"",

	"name": "CastError"

	},

"statusCode": 500

}
```

* **Sample Call:**

DELETE /api/v1/article/delete/5e01441e9cb84346381c355 HTTP/1.1
Host: localhost:3000
API-TOKEN: 5CD4ED173E1C95FE763B753A297D5
Content-Type: application/x-www-form-urlencoded
User-Agent: PostmanRuntime/7.20.1
Accept: */*
Cache-Control: no-cache
Postman-Token: 4f062f18-f0f6-43db-b33a-043cd17ffb8f,1faa7690-c7f3-4dfb-b825-3cca6452db9d
Host: localhost:3000
Accept-Encoding: gzip, deflate
Content-Length: 0
Connection: keep-alive
cache-control: no-cache


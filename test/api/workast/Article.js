const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../../app.js');
const token = '5CD4ED173E1C95FE763B753A297D5';
let articleId = null;
let userId = null;

describe('GET /api/v1/article/get', () => {
  let server;
  before((done) => {
    server = app.listen(3000);
    done();
  });

  after((done) => {
    server.close();
    done();
  });

  it('OK, Getting Articles', (done) => {
    request(app)
        .get('/api/v1/article/get')
        .query({tags: 'MUSIC, NEWS'})
        .set('api-token', token)
        .then((res) => {
          expect(res.statusCode).to.equal(200);
          expect(res.body.error).to.equal(false);
          expect(res.body.statusCode).to.equal(200);
          expect(res.body).to.contain.property('data');
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);

  it('Fail Authorization, Getting Articles', (done) => {
    request(app)
        .get('/api/v1/article/get')
        .query({tags: 'MUSIC, NEWS'})
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);
});

describe('POST Creating Article', () => {
  let server;
  before((done) => {
    server = app.listen(3000);
    request(app)
        .post('/api/v1/user/add')
        .set('api-token', token)
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({avatar: 'http://www.google.com', name: 'workast'})
        .then((res) => {
          userId = res.body.item._id;
          done();
        });
  });

  after((done) => {
    server.close();
    done();
  });

  it('Ok, Creating Article', (done) => {
    request(app)
        .post('/api/v1/article/add')
        .set('api-token', token)
        .send({text: 'Esto es una prueba', title: 'prueba',
          userId: userId, tags: ['MUSIC', 'FOOTBALL', 'NEWS']})
        .then((res) => {
          expect(res.statusCode).to.equal(201);
          expect(res.body.error).to.equal(false);
          expect(res.body).to.contain.property('item');
          articleId = res.body.item['_id'];
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);

  it('Fail Authorization, Creating Article', (done) => {
    request(app)
        .post('/api/v1/article/add')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({'text': 'Esto es una prueba', 'title': 'prueba',
          'userId': userId, 'tags': ['MUSIC', 'FOOTBALL', 'NEWS']})
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);
});

describe('PUT Editing Article', () => {
  let server;
  before((done) => {
    server = app.listen(3000);
    done();
  });

  after((done) => {
    server.close();
    done();
  });

  it('PUT, Editing Article', (done) => {
    request(app)
        .put('/api/v1/article/edit/' + articleId)
        .set('api-token', token)
        .send({'text': 'Esto es una prueba'})
        .then((res) => {
          expect(res.statusCode).to.equal(202);
          expect(res.body.error).to.equal(false);
          expect(res.body.statusCode).to.equal(202);
          expect(res.body).to.contain.property('item');
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);

  it('Fail Authorization, Editing Article', (done) => {
    request(app)
        .put('/api/v1/article/edit/5dff8bd8c757a6620d117622')
        .send({'text': 'Esto es una prueba'})
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);
});

describe('DELETE, Delete Article', () => {
  let server;
  before((done) => {
    server = app.listen(3000);
    done();
  });

  after((done) => {
    server.close();
    done();
  });
  it('OK, Article Deleted', (done) => {
    request(app)
        .delete('/api/v1/article/delete/' + articleId)
        .set('api-token', token)
        .then((res) => {
          expect(res.statusCode).to.equal(202);
          expect(res.body.error).to.equal(false);
          expect(res.body.deleted).to.equal(true);
          expect(res.body.statusCode).to.equal(202);
          expect(res.body).to.contain.property('item');
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);

  it('OK, Article not found', (done) => {
    request(app)
        .delete('/api/v1/article/delete/' + articleId)
        .set('api-token', token)
        .then((res) => {
          expect(res.statusCode).to.equal(404);
          expect(res.body.error).to.equal(true);
          expect(res.body.message).to.equal('item not found');
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);

  it('Fail Authorization, Deleting Article', (done) => {
    request(app)
        .delete('//api/v1/article/delete/' + articleId)
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);
});

const expect = require('chai').expect;
const request = require('supertest');

const app = require('../../../app.js');
const token = '5CD4ED173E1C95FE763B753A297D5';

describe('POST /api/v1/user/add', () => {
  let server;
  before((done) => {
    server = app.listen(3000);
    done();
  });

  after((done) => {
    server.close();
    done();
  });

  it('OK, Creating User', (done) => {
    request(app)
        .post('/api/v1/user/add')
        .set('api-token', token)
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({avatar: 'http://www.google.com', name: 'workast'})
        .then((res) => {
          expect(res.statusCode).to.equal(201);
          expect(res.body.error).to.equal(false);
          expect(res.body).to.contain.property('item');
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);

  it('Fail Authorization, Creating User', (done) => {
    request(app)
        .post('/api/v1/user/add')
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .send({avatar: 'http://www.google.com', name: 'workast'})
        .then((res) => {
          expect(res.statusCode).to.equal(401);
          done();
        })
        .catch((err) => {
          done(err);
        });
  }).timeout(5000);
});

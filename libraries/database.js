const mongoose = require('mongoose');
const pino = require('pino')();
const pinoCaller = require('pino-caller')(pino);
/**
* Database Class
*/
class Connection {
  /**
  * I make this class Global, The connection with Mongo will be stablished
  * just once
  *
  */
  constructor() {
    const url =
      process.env.MONGODB_URI || `mongodb://mongo:27017/workast`;
    console.log('Establish new connection with url', url);
    mongoose.Promise = global.Promise;
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    mongoose.set('useUnifiedTopology', true);
    try {
      mongoose.connect(url);
    } catch (err) {
      pinoCaller.error(`Error ${err}`);
      process.exit(1);
    }
  }
  /**
   * Close Connection
   */
  close() {
    mongoose.connection.close();
  }
}

module.exports = Connection;

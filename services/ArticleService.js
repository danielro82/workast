// src/services/PostService
const Service = require('./Service');
const sanitize = require('mongo-sanitize');
const mongoose = require('mongoose');
/**
* Article Service
*/
class ArticleService extends Service {
  /**
   *
   * @param {*} model
   */
  constructor(model) {
    super(model);
  }

  /**
   * Redefined for sanitazing body
   * @param {*} data
   */
  async insert(data) {
    Object.keys(data).forEach((param) => {
      data[param] = sanitize(data[param]);
    });
    const error = {
      error: true,
      statusCode: 500,
      errors: ['User doesn\'t exists'],
    };
    try {
      const response = await mongoose.model('users')
          .find({_id: data.userId}, {runValidators: false});
      if (!response.length) {
        return error;
      }
    } catch (err) {
      return error;
    }
    return super.insert(data);
  }
  /**
    * Override update for parsing tags string to array, pre findOneAndUpdate
    * not support parameters
    * @param {*} id
    * @param {*} data
    */
  async update(id, data) {
    id = sanitize(id);
    Object.keys(data).forEach((param) => {
      data[param] = sanitize(data[param]);
    });
    const error = {
      error: true,
      statusCode: 500,
      errors: ['User doesn\'t exists'],
    };
    try {
      const response = await mongoose.model('users')
          .find({_id: data.userId}, {runValidators: false});
      if (!response.length) {
        return error;
      }
    } catch (err) {
      return error;
    }
    return super.update(id, data);
  }

  /**
    * Override update for parsing tags string to array
    * not support parameters
    * @param {*} id
    */
  async delete(id) {
    id = sanitize(id);
    return super.delete(id);
  }

  /**
    * Override getAll method to search for array tags parameter
    * @param {*} query
    */
  async getAll(query) {
    if (query.tags === undefined) {
      return {
        error: true,
        statusCode: 500,
        errors: ['Param tags is mandatory'],
      };
    }
    query.tags = sanitize(query.tags).split(',').map((el) => el.trim());
    query = {'tags': {$all: query.tags}};
    return super.getAll(query);
  }
};

module.exports = ArticleService;

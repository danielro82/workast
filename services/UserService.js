// src/services/PostService
const Service = require('./Service');
const sanitize = require('mongo-sanitize');
/**
* User Service
*/
class UserService extends Service {
  /**
   *
   * @param {*} model
   */
  constructor(model) {
    super(model);
  }

  /**
    * Get documents
    * @param {*} data
    */
  async insert(data) {
    Object.keys(data).forEach((param) => {
      data[param] = sanitize(data[param]);
    });
    return super.insert(data);
  }
};

module.exports = UserService;

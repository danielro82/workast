# Workast Challenge done by Daniel Edmundo Rodríguez López Serra


##Most useful tools versions
  - NodeJS v9.2.1
  - MongoDB shell version v3.4.4


###### Most important modules used:
  - **APICACHE**: It's used to cache for five minutes the response of API /article/get for testing purposes.
  - **Pino, pino-caller, express-pino-logger** are used for the logging.
  - **MONGO-SANITIZE**: It's used to sanitize the body, url and query string parameters.
  - **express-group-routes**: It's used for route grouping.
 
 ###### Important environmental variables
  - **apiToken**: It contains the token used in the header (called api-token)
    for default it's used 5CD4ED173E1C95FE763B753A297D5.
  - **NODE_ENV**: Environment, development o (default) production.
  - **MONGODB_URI**: It contains the mongo server URL. by default it's "mongodb://mongo:27017/workast" (mongo means the database name in docker).
  -**PORT**: Port running the API. Default is setting to 3000. 
  ## Running the API.
   Inside the repo of the challenge, there are a Dockerfile and a Docker compose file.
   To build and up the container you need to run the following commands:
  ```sh    
      sudo docker-compose build
      sudo docker-compose up
   ```
   The API is runned by PM2 inside the Docker Container.
   There is a file called ecosystem.config.js which holds the info about the process.
   
   If you don't wish to run the API through Docker, you can run locally from the repo source by;
   
   ```sh    
      npm install
      npm start
   ```

### Unit Testing

The unit tests are located in the tests folder.
for running the test you have to run
```sh    
      mocha test/api/workast/User.js
      mocha test/api/workast/Article.js
   ```

### APIS DOC

The following links aim to the api documentation

| API | README |
| ------ | ------ |
| User Create | [USER-CREATE.md](/USER-CREATE.md) |
| Article Create | [ARTICLE-CREATE.md](/ARTICLE-CREATE.md)  |
| Article Update | [ARTICLE-UPDATE.md](/ARTICLE-UPDATE.md)  |
| Article Delete | [ARTICLE-DELETE.md](/ARTICLE-DELETE.md)  |
| Article Get | [ARTICLE-GET.md](/ARTICLE-GET.md)  |
